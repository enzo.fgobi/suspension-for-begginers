---
title: "Básico de Geometria"
date: 2022-10-09T17:41:35-03:00
draft: false
---
# Bitola e Entre Eixos
#### Bitola é a distância entre os centro dos pneus avaliados no eixo Y (bem comum de ter valores distintos para a dianteira e traseira)

![bitola](http://2.bp.blogspot.com/-8W5Kb6M55X4/TZUqJocawtI/AAAAAAAABTs/bTeJp9aECkM/s1600/Bitola+eixo+traseiro.jpg)

#### Entre eixos é a distância entre os centros das rodas avaliados no eixo X

![entreeixos](https://www.researchgate.net/profile/Diego-Bravo-6/publication/301689421/figure/fig1/AS:355889975447555@1461862190778/Figura-28-Bitolas-e-entre-eixos.png)

# Centro de gravidade

#### O centro de gravidade também é conhecido como centro de massa, é a posição média considerada com relação a massa de todas as partes de um objeto ou sistema

![CoG](https://imagizer.imageshack.com/v2/800xq90/922/B0eIET.jpg)

# Cambagem

#### É O ÂNGULO FORMADO ENTRE O PLANO DO PNEU E A VERTICAL

![Cambagem](https://www.carvizion.com.br/uploaded-images/shares/5bb645865a2ca.png)

# TOE 

#### É O ÂNGULO FORMADO ENTRE O PLANO DO PNEU E O EIXO CENTRAL DO CARRO

![toe](https://3.bp.blogspot.com/-rZY3T7p7Rbk/XFhfnAoIUdI/AAAAAAAAC4M/6pFuqQ4w5IUkqeo5xiChgjhxFmruj26XQCLcBGAs/s1600/toe%2Bin%2Btoe%2Bout%2B.jpg)

# Caster

#### É O ÂNGULO FORMADO ENTRE O PLANO DO PNEU E O EIXO LATERAL DO CARRO

![Caster](https://acervo.avozdaserra.com.br/sites/default/files/colunas/90-caster.jpg)