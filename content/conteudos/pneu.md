---
title: "Pneu"
date: 2022-10-09T17:41:44-03:00
draft: false
---
## Grip


![penu](https://d3nv2arudvw7ln.cloudfront.net/images/global/593/6/93649-new-pzero-slick-3-4-v2-1505470084502.jpg)



#### O estudo do Pneu se inicia na compreensão das interações químicas e mecânicas do pneu com o solo, sendo esses os dois mecanismos de geração de força, o mechanical grip, ou seja, a reação das forças normais às irregularidades entre ambos os corpos, comumente abordado em literaturas como gearing, ou em português como: engrenamento 

.
.
.


![Gearing](https://www.fq.pt/images/forcas/forca-de-atrito.png)

.
.
.

#### Além do grip gerado mecanicamente também há uma geração de interações químicas, sendo em pneus de performance o maior responsável pela geração de forças e momentos, tais interações decorrem das "van der waals" que surgem entre o pavimento e a borracha do pneu, assim como a borracha do pneu e os resquícios de borracha depositados no pavimento 

